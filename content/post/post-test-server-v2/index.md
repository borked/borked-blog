---
title: "Post Test Server V2"
date: 2022-04-15T20:56:50-04:00
draft: false
---

The past few days I've been working on testing to see if the following workflow would be possible within a particular SaaS platform:

1. Trigger a script to run when a particular event occurred.
2. Pull data about the event into my script.
3. Push that data to an external system.

It's a good bit more complex than that, but determining if this scenario was even possible through the platform in question was the high-level goal. After a _lot_ of documentation review, I was able to succeed with items #1 and #2. The third part was a little trickier because I don't yet know how to use the system where data would actually be pushed to if this turns from a hypothetical into something real. Since my code is essentially executing from a SaaS application, I don't have the ability to do logging or write to standard out; all I can see is if it "succeeded" or "failed" from the perspective of whether or not it completed with an non-zero exit code.

So I needed something I could quickly test pushing dummy data into which would also give me the ability to _view_ that data external to the system my code was executing from in order to verify that everything actually worked; I didn't really want to spend any time even setting up the world's simplest Flask application for this. After some searches, though, I stumbled across [Post Test Server V2](https://ptsv2.com/s/whatisthis.html). You can read the linked page for -- humorously written -- details on what exactly the service is, but the tl;dr is that it offers up an API endpoint doing exactly what I needed.

Going to the [homepage](https://ptsv2.com/) gives you the option to generate a new random "toilet." The toilet will essentially have a random route and will allow you to make an HTTP POST to it at:

```
https://ptsv2.com/{toilet-id}/post
```

The toilet houses the "dumps", i.e. the data you dump into it with your post. Yes, the naming is terrific, and this trend will continue. By default, anyone with the URL can post. Likewise, anyone with the URL can view the data, and the site clearly states that the data is saved in plaintext. So you do _not_ want to use this with anything sensitive. But if you're just testing sending out garbage data from something that doesn't give you good local visibility? This is perfect. You do get the option to configure some basic authentication, change the response status -- if you need to test different codes other than 20X, and change the response body.

![Screenshot of the PTSV2 configuration options described above](/post/post-test-server-v2/post_test_server_config.png)

I didn't need any of that, though writing this makes me realize I should test what happens if my post "fails" according to the HTTP response; I'll mentally add that to my docket for Monday morning. After pulling the trigger on my script, I confirmed I could see exactly what I expected on PTSV2. After refreshing the page, I get a list of my current dumps:

![Screenshot of the PTVS2 toilet view listing 2 dumps](/post/post-test-server-v2/results.png)

Clicking a dump gives you a slew of information, though in my case I just wanted to verify that the body had contained the fake server names I had passed along:

![Screenshot of the details of a PTSV2 dump showing that the Post Body value contained a list of servers](/post/post-test-server-v2/post_body.png)

If you want to clean up after yourself, you can click the button to "flush" a dump or the button to flush all of your dumps on the overview page... yes, I said the trend was going to continue.

I'm really geeked that someone offers a service like this free of charge, and that it even does a little bit of data retention. I have no idea how long items will stay in a toilet, but my test data survived the duration from early this afternoon when I was testing with it until early this evening so I could take screenshots for this post. I have no doubt this will come in handy again in the future.
