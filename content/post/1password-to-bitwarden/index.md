---
title: "1Password to Bitwarden"
date: 2022-04-10T20:27:46-04:00
draft: false
---

## Background

Friday was my last day at the job I've worked for the past two and a half years. While I'm excited to start the next chapter of my career on Monday, I needed to figure out what to do about my passwords first. My previous employer had a [1Password Business](https://1password.com/teams/pricing/) subscription, which includes a "free" Family Account to encourage people to make a second account with completely separate vaults for anything in their personal life rather than storing those in their work vault.

_Note: as someone who managed this 1Password account, this stopped absolutely no one from just storing all of their personal credentials in their work vault, causing chaos if they left/lost their job._

When my work account gets suspended, my personal vault would still exist but be inaccessible until I pay up for it. This is actually the _exact_ situation I ran into when I left my previous job. On my first day while I'm in the middle of filling out paperwork with HR, my old 1Password Business account got suspended, and I had to whip out my credit card in order to re-subscribe for access to my personal vault so that I could set up accounts in all of the new systems I needed to access. When *that* employer got their own 1Password subscription, I was able to shift back to the Family account I could access for free that way.

Figuring I didn't want to run into the exact same scenario again, I decided I would handle the situation _before_ it became an issue. First I needed to decide which password manager I would use. The very first password manager I ever used was [LastPass](https://www.lastpass.com/). Back at this time, 1Password wasn't even an option for me -- I had looked into it because one of my friends was a huge fan -- because they didn't have support for Linux or Chromebooks given that a desktop app was required. After LastPass had a [security breach](https://www.pcworld.com/article/428084/the-lastpass-security-breach-what-you-need-to-know-do-and-watch-out-for.html), though, I decided to look for something different; their subsequent [sale to LogMeIn](https://www.zdnet.com/article/lastpass-bought-by-logmein-for-110-million/) only cemented the idea that I made the right decision in jumping ship.

Next, I ended up using [Bitwarden](https://bitwarden.com/). It's an [open source offering](https://github.com/bitwarden) that gives full functionality with free accounts, including cloud sync and access from mobile and a browser. I was a big fan and used it for several years before my employer at the time purchased a 1Password Business subscription. I exported my Bitwarden contents to a CSV file and imported those into 1Password. I continued using it right up until about 30 minutes ago, going through some of the aforementioned hurdles as I lost and re-gained access to Business accounts.

I decided I would go back to Bitwarden rather than just paying for 1Password again. I like the idea of leveraging open source projects. While the free offering meets my needs, the [Personal plan](https://bitwarden.com/pricing/) is just $10 USD per _year_. I think that's worthwhile to both:

1. Support the project
2. Gain access to their exposed password report and 1 GB of secure file storage

## Switching

The last time I switched password managers, the order of operations was:

1. Use functionality in the current password manager to export a CSV file of all content.
2. Find the documentation for the new password manager to determine what format it expected for the CSV file with respect to headings and supported fields.
3. Massage the file from Step 1 to match what was expected from the documentation in Step 2.
4. Import the updated CSV file into the new password manager and hope for the best.

Things got off to a bit of a rough start initially because I discovered that 1Password doesn't support [exporting content from the web interface](https://www.reddit.com/r/1Password/comments/dfhmfb/how_to_export_data_from_1password_x/). If you want to export anything, you _must_ [use one of their desktop apps](https://support.1password.com/export/). This is fairly BS in my opinion as they could easily add this functionality to the web. Fortunately, they now have Linux support, so I could install their `.deb` file, which I used just to log into my account and click the export button. I then immediately uninstalled the app.

When clicking the export button, I had the choice between picking a CSV file or a proprietary 1PUX file. The difference between the two is that the CSV file will only capture the basic login information while the 1PUX file will capture all data, both extraneous to logins (e.g. additional fields I added, which is helpful since I often just created custom fields for things like API keys) and separate things like secure notes.

![1Password export utility showing the CSV and 1PUX options](/post/1password-to-bitwarden/1pass_export.png)

I at first exported a CSV assuming that would be my only option. However, I was pleasantly surprised when I went to upload this to Bitwarden that I was able to select a 1PUX, which is confirmed by their [documentation](https://bitwarden.com/help/import-from-1password/).

![Screenshot showing 1PUX as the upload file type selected for importing passwords](/post/1password-to-bitwarden/bitwarden_import.png)

After a few brief moments, the 1PUX file was successfully processed and all of my credentials, secure notes, etc. appeared in Bitwarden. Then I just had to remember to delete the 1PUX file from my local machine considering it contains the access to everything in my life unencrypted. You may also notice from the screenshot above that, unlike 1Password, Bitwarden supports both import _and_ export from their web interface, which is a nice touch.

## The Follow-up

After this was squared away I just needed to get the Bitwarden apps for everything. On both my iPhone and iPad this was as simple as:

1. Install the app from the app store.
2. Log in.
3. Enable unlock with Face/Touch ID from the Bitwarden settings.
4. Enable Bitwarden as a source for auto-fill in the iOS/iPadOS settings.

On my laptop, currently running [Pop!_OS](https://pop.system76.com/), I first installed the [Firefox extension](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search). Checking Bitwarden's [download page](https://bitwarden.com/download/), there _is_ Linux support, but the installation from a `.deb` seems to be deprecated in favor of either AppImage or Snap, both of which I mostly hate. I decided that I'll stick with the Firefox extension for now and will circle back to biting the bullet if I feel that I _really_ need a dedicated local client; it seems unlikely since I was fine using the Firefox extension for 1Password.

![Screenshot of the Bitwarden download page's Linux section showing that .deb and .rpm are deprecated in favor of AppImage or Snap.](/post/1password-to-bitwarden/bitwarden_linux_clients.png)

I don't really have any next steps since this was a simple, painless process, but at some point in the future I'll probably try playing around with their [CLI](https://bitwarden.com/help/cli/).
