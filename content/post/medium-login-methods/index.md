---
title: "Medium Login Methods"
date: 2022-04-25T21:02:49-04:00
draft: false
---

This post can be categorized under "Things so simple they shouldn't require a blog post", but given the amount of time I just spent scratching my head over this, I figured someone else could easily find themselves in the same boat. If nothing else, it gives me an excuse to create yet another blog post.

I have a [blog over at Medium](https://borkedsh.medium.com/) that I used for a period of time prior to creating this one on [Write.as](https://write.as/). Medium is actually where I moved my content to when I decided to ditch WordPress, which is what I used when I decided to ditch Squarespace, which is what I had moved to when I didn't have enough web design chops to keep using [Hugo](https://gohugo.io/) for what I needed. Suffice to say, that content has shuffled around quite a bit. Learning Hugo properly is also still on my radar (I have a copy of [Hugo In Action](https://www.manning.com/books/hugo-in-action?query=hugo%20in%20action) which I pre-ordered, and the final version of the book was _just_ released), though I don't see that supplanting this blog any time soon.

I had briefly thought about trying to move all of my old content over here, but ultimately I was looking for something of a fresh start with this blog. However, I don't want to _lose_ several years' worth of posts or cut off my own access to them -- there's honestly some useful material there that I still find myself referencing periodically -- hence why I kept the Medium account around rather than deleting it. I don't plan to go back to using Medium, but I'd prefer to keep the account accessible just in case. However, I created that particular account by authenticating to Medium via Twitter. That rapidly became something of a conundrum -- or so I thought -- when I finally decided today to nuke all of my Twitter accounts in light of who just acquired the company. I needed to swap my Medium login from Twitter to something different, preferably just a username/email address and password. However, looking at the Medium settings didn't show me any means by which to do this:

![Screenshot of the security settings of the Medium website showing options for signing out of existing sessions, deactivating the account, and deleting the account.](/post/medium-login-methods/security_options.png)

After digging through the other settings convinced I had to be missing something, I found no options for setting up another authentication method. I took to the Internet, but searches for things like...

> medium switch from twitter authentication

... and...

> medium blog change authentication

... gave me nothing relevant. Still in the settings of my Medium account, I clicked the button under **Connections** to unlink my Twitter account, thinking this would prompt me to pick a different authentication method. Instead, it happily permitted me to unlink my account with no further input.

![The Connections portion of the Medium settings page showing Twitter as not being linked.](/post/medium-login-methods/connections.png)

The plot thickens. While poring through the Settings, I also noticed that under the **Email settings**, I did have an email address populated. This is the address which Medium sends notification to. I was curious as to what would happen if I attempted to log in via email and then initiate a forgotten password request. Not wanting to risk logging out while Twitter was unlinked lest I accidentally lock myself out of the account (I could always restore Twitter within 30 days, but I really didn't want to go through that), I opened a private browser instance and went over to Medium.

Medium offers up quite the list of login options; they clearly want to direct people to use authentication other than directly through them, and I would soon realize just how true that was.

![Screenshot of the Medium login screen that has options for Google, Facebook, Apple, Twitter, and email](/post/medium-login-methods/sign_in.png)

I clicked email and was briefly surprised there wasn't an option for a forgotten password before I realized it was because there _is no password_ with this method; Medium simply emails you a magic link you click on.

![Screenshot of the email login option through Medium which states "Enter the email address associated with your account, and we'll send a magic link to your inbox."](/post/medium-login-methods/email_login.png)

As you might guess, it was right around _this_ point that I started to feel like a bit of an idiot. If you have an email address associated with your Medium account, you can always just log in by clicking the email option and receiving a magic link to your inbox... there isn't any email address and password authentication mechanism in the site.
