---
title: "Podman And Minikube"
date: 2022-05-16T18:44:31-04:00
draft: false
---

I recently needed to do some testing with deploying an application into a Kubernetes cluster. For my local container work, I've recently taken to using [Podman](https://podman.io/), which works great on macOS alongside of [QEMU](https://www.qemu.org/). Both of them are available on [Homebrew](https://brew.sh/), meaning they can easily be installed via:

```
brew install podman qemu
```

Likewise, it's easy to keep them updated -- along with _all_ of my other Homebrew packages -- through:

```
brew update && brew upgrade
```

Today, however, I needed to go beyond standalone containers and do some testing with Kubernetes. I briefly debated using [Rancher Desktop](https://rancherdesktop.io/) to give me something akin to what I used to use with [Docker Desktop](https://www.docker.com/products/docker-desktop/) prior to their licensing change, but it felt a bit unnecessary to me. Other than a checkbox in Docker Desktop to enable Kubernetes, I never did anything with the UI; everything was still through the CLI. Thus, Rancher seemed overkill when I already had Podman for my container level.

Instead, I decided to give [minikube](https://minikube.sigs.k8s.io/docs/) a try, at least in part because it's **also** available via Homebrew:

```
brew install minikube
```

Half reading the [start doc](https://minikube.sigs.k8s.io/docs/start/) and half assuming I would figure it out, I first ran:

```
minikube start
```

This gave me an error:

> Exiting due to DRV_NOT_DETECTED: No possible driver was detected. Try specifying --driver, or see https://minikube.sigs.k8s.io/docs/start/

Right, if I want to use Podman for my driver, I have to **tell** it that. That was easy enough via the `--driver` parameter:

```
minikube start --driver=podman
```

_**Note:**_ This can also be accomplished by setting the default driver. I went ahead and ran the below command at the end regardless just in case I end up making additional VMs in the future:

```
minikube config set driver podman
```

While the driver ceased to be a problem, I still got an error when trying to start minikube:

> Exiting due to RSRC_INSUFFICIENT_CORES: Requested cpu count 2 is greater than the available cpus of 1

The error couldn't be more clear, and this was just a matter of my not reading closely enough as the requirements state that I'll need:

- 2 CPUs or more
- 2GB of free memory
- 20GB of free disk space
- Internet connection
- Container or virtual machine manager, such as: Docker, Hyperkit, Hyper-V, KVM, Parallels, Podman, VirtualBox, or VMware Fusion/Workstation

To check what I was currently working with, I looked at my current Podman VM with: `podman machine list`

That gave me:

```
NAME                     VM TYPE     CREATED                 LAST UP            CPUS        MEMORY      DISK SIZE
podman-machine-default*  qemu        Less than a second ago  Currently running  1           2.147GB     107.4GB
```

You can see here that I only have one VM, the default one, and it was allocated 1 CPU by default. Kubernetes wants a bit more muscle behind it, so I opted to recogfigure it. Note that I don't know for sure if I had to stop the VM first, but I just kind of assumed that I did:

```
podman machine stop
podman machine set --cpus 2 --rootful
podman machine start
```

After this, I was able to run `minikube start --driver=podman` without any issues.

I'm still getting used to Kubernetes, but so far I've been enjoying it. I'm getting the hang of the deployment YAML and setup scripts, which have been super fun to learn.
