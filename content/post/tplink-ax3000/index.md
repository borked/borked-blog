---
title: "TP-Link AX3000"
date: 2022-04-11T20:47:16-04:00
draft: false
---

## The Ode To Failure

_If you just want to know about the new router, feel free to skip this entire section._

For the past several years, I've had a pretty solid home networking setup, featuring a [FortiGate](https://www.fortinet.com/products/next-generation-firewall) as my layer 3 router, an [Extreme](https://www.extremenetworks.com/) access point for wireless, and a double-NAT'd [Cradlepoint](https://cradlepoint.com/) that I connected just my work gear to for LTE failover. Such were the perks of being employed by a networking company. Since I mentioned in my [last post](https://choochooboom.com/1password-to-bitwarden) that I had accepted a new job, though, it meant I removed all of the gear running my home network. Looking through what I had left over to be my routing and wireless solution, I needed to decide between:

1. The ancient router given to me by my ISP
2. Google WiFi

The router from my ISP, the brand of which I couldn't even tell you without digging it out of a box buried in my closet and looking, is terrible. I actually told them when I signed up for service after moving to this location that I didn't need a router from them, but they gave me one anyway. Since they aren't charging me for it, I've been content to leave it rotting in the aforementioned closet. I actually liked my Google WiFi setup back when I bought it; while I only had a 900 square foot apartment at the time, having a 3 node mesh network ensured I that I asserted **utter WiFi dominance** over a very crowded airspace. These days, however, I'm not a fan of it. For one, I decided that Google has taken a bit of a more evil slant than I'd like. Secondly, I've run into _heinous_ issues with it before since it can _only_ be configured via a mobile app. I won't go into the full details since it would be enough material for a lengthy post in its own right -- a post I've made elsewhere and am choosing to not link here -- the tl;dr is that if the access points can't be reached via the mobile app, you're more or less screwed. I've wasted hours of my life staring at this:

![The Google WiFi mobile app attempting to connect to an access point](/post/tplink-ax3000/google_wifi_fail.png)

Attempting to connect to the web interface of a device will work... but only to tell you that you need to use the app:

![Google WiFi access point's web interface instructing to use the mobile app](/post/tplink-ax3000/google_wifi_web.png)

This was an absolute rage factory for me, but... it still seemed better than the router from my ISP. So I started reconnecting my Google WiFi. When I stored the 3 APs after the last time I used it, I actually placed them in a _specific_ order so that I would know which was already configured to act as the router and which were configured to just be members of the mesh network, as that would save me from having to go through the agony of factory resetting them... or so I thought.

The first device, designated as the gateway, connected just fine and began to broadcast the WiFi network that was last configured with it. I had no clue what the PSK was, so I installed the Google Home app, which replaced the dedicated Google WiFi app. Despite the fact that the devices were registered into the Google Home app previously, though, they didn't show up when I opened it... which meant that I couldn't check the PSK. So I ended up having to factory reset the primary device regardless. That at least went by relatively painlessly, and within 10 minutes I was able to connect my phone to the network being broadcasted while the device was managed via Google Home. Knowing a factory reset would be required, I then started that process on the second AP, which was just going to be a part of the mesh network. Despite this, however, I couldn't get it to connect to the mobile app. The attempt would simply fail repeatedly, despite the fact that my iPhone was connecting to the device's temporary SSID without any issues.

I tried to connect to the 3rd AP but got the same experience as connecting to the second. The last time this happened, I ran into problems with one of the APs when using my phone, so I grabbed an iPad and repeated the process without any luck. Several additional factory resets later and I realized this was in no way worth my time considering I didn't want to keep using Google WiFi in the long run regardless. Instead, I left _just_ the node acting as the gateway running and disconnected the 2 mesh APs that didn't want to cooperate while I hopped online to start doing some quick research.

## Networking Catharsis

My primary objective in looking for a new router was to find something which would allow me to web to the device's local UI and configure it from there; my Google WiFi setup issues would've vanished if connecting to the local web interface did more than digitally giving me the finger. Bonus features included WiFi 6 support and -- while not a feature -- something that wasn't egregiously expensive since, in my experience, consumer networking gear doesn't exactly have the best longevity. Something I could get sooner rather than later was also preferable since I wanted to have this set up in my network and functioning for a while _before_ starting my new job. Several friends of mine, the technical acumen of whom I highly respect, are quite the fans of [Eero](https://eero.com/). Much like Google's offerings, however, Eero can also only be configured via a mobile app, and frankly I'm not doing that again until the universe drags me there by not offering anything else... at which point I'll start buying small office gear for home.

After a couple of hours of digging through what was available and then doomscrolling/doomscrubbing through reviews and videos, I settled on the [TP-Link Archer AX3000](https://www.tp-link.com/us/home-networking/wifi-router/archer-ax3000/). It immediately looked fairly enticing to me since it comes in relatively cheap (and had an additional $15 off on Amazon at the time.) While I've never owned anything by TP-Link before, a friend and (now) former coworker of mine who is Really Good At Networking™ had previously posited the idea of using TP-Link gear for some low-cost switching ventures; if their gear is good enough for him to consider selling, it has to be good enough for my home network.

Setup was a bit of a concern since every product listing has something akin to the official one:

> Easy Setup: Set up your router in minutes with the powerful TP-Link Tether App

However, after some research I confirmed that you can use the app _or_ the local interface to configure the device; this is the perfect scenario for me since I'm not _opposed_ to having an app available... I just don't want it to be the only method. It also supports TP-Link's "[OneMesh](https://www.tp-link.com/us/onemesh/)" if I felt the need to expand my network, though that seems unlikely in my (different but still small) apartment. To top things off, I could even get overnight shipping for free at the time from Amazon.

![Photo of the TP-Link AX3000 sitting in my home](/post/tplink-ax3000/tplink_ax3000.jpg)

Setup was extremely easy, even for a networking scrub like myself. While I broke out an Ethernet cable, I didn't even need it. By default the device broadcasts an SSID behind a PSK, both of which are on a sticker on the bottom of the router. It was simple enough to connect to that SSID, web to the local interface, set up a new admin password, and change the wireless configuration to what I wanted. I also tweaked a few other things like changing the DNS servers, creating DHCP reservations for a couple of devices, etc. I think it's also worth noting that the instructions which came in the box with the device made it quite clear that both the app and local web UI were options for configuring the device, which I appreciated it.

The coverage provided by the AX3000 has been fine for my use. Again, I live in a relatively small apartment, but some devices I've used have struggled to provide a decent signal to the opposite side of my unit due to the crowding that happens in the channels with so many people living in close proximity. Some people in reviews were pretty upset that TP-Link tries to upsell you on their [HomeShield](https://www.tp-link.com/us/homeshield/) subscription service, but I don't lose any basic functionality by not having that; it's just a tab in the UI that I never click on. I was just pleasantly surprised that the device offers VPN support so that I can remotely connect back to my home network if I choose to configure it; I don't have any need to do so, but I like having the option regardless.

Will the AX3000 knock the pants off of someone used to enterprise solutions? 100% no. Will it do an acceptable job of running a home network while providing basic features, solid coverage, and WiFi 6 connectivity (for what that's worth)? Absolutely. I feel like I could've done a _lot_ worse in selecting something to power my home network considering I've had zero complaints for my fairly basic set of needs. In a world of flaky connections with mandatory mobile apps, if reconnecting my IoT devices to the network takes more time than setting up the network in the first place, I'm going to count that as a win.
