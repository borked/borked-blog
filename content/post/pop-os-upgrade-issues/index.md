---
title: "Pop!\_OS Upgrade Issues"
date: 2022-05-14T08:42:16-04:00
draft: false
---

## Background

For the past few years, I've been quite the fan of [Pop!\_OS by System 76](https://pop.system76.com/). Despite the weird moniker, I found it to be a terrific operating system. It's a derivative of [Ubuntu](https://ubuntu.com/), which is by far the Linux flavor I have the most experience with -- it was also the very first Linux distro I ever installed back with 6.06 "Dapper Drake" -- which makes using Pop!\_OS a breeze... typically. I could administer it the same way I would Ubuntu, but since it comes from a hardware vendor, I found it to have significantly better out of the box hardware support than Ubuntu did. This was especially important for me previously when I was doing a lot of gaming on Linux. While I don't do too much gaming these days unless you count [online chess](https://lichess.org/@/LoopedNetwork), I still have an affinity for Pop!\_OS, and I much prefer its UI to Ubuntu's; I mainly use Ubuntu for servers these days.

## The Process

I had been running Pop!\_OS on my main pesonal laptop for quite a while, using the 21.10 release at the time, so I was excited when I saw news of the [latest 22.04 release](https://blog.system76.com/post/682519660741148672/popos-2204-lts-has-landed) finally being available. I knew it was only a matter of time since Ubuntu had launched their 22.04 shortly beforehand. Since I've seen this particular pitfall before, I first installed all of the available packages for the 21.10 release prior to attempting the upgrade. Everything went smoothly after a little:

```
sudo apt update && sudo apt upgrade
```

After updates had completed and I had gone through a reboot, I kicked off the 22.04 update via the GUI. I don't normally use the GUI for this sort of thing, but for whatever reason I never remember the CLI command to upgrade the OS for Ubuntu. I think it's `do-release-upgrade` or something like that, but rather than looking I just lazily clicked the button in the UI.

_Note: I'm not saying the GUI caused the problems that will follow. I'm just embarrassed I did things this way because... gross._

The UI showed that it upgraded my recovery partition, downloaded 22.04, and then started the update process. Shortly after that, it needed a reboot, which I happily did. After booting back up, it went through several screens that sure _looked_ like they were upgrading my OS. After just a few short minutes, it said the update was complete and rebooted once again. When I logged in, though, everything looked... exactly the same. Even worse, though, was that the UI immediately gave me a notification that the 22.04 release was available for me to install. Running...

```
lsb_release -a
```

... confirmed for me that I was still ostensibly running 21.10. I kicked off the installation **again**, and the whole process outlined above repeated itself exactly as before, including booting back to 21.10 and prompting me to install 22.04. At this point my thought was that something was amiss with the GUI update process and that I would attempt the installation a 3rd time from the CLI like normal. (This is why I had to come clean above and couldn't just lie about how I kicked off the update if I wanted the post to make any sense. 😅)

## The Mistake

Back in my Terminal, I decided on a whim to make sure there were no additional package waiting for updates. I re-ran the `apt` commands and was greeted with a **mountain** of packages that had updates. This was immediately a red flag because it seemed like every package had an update, exactly like what you'd expect to occur while the OS was upgrading. The smart decision would have been to _not_ install those updates and go through some troubleshooting instead.

The problem is that I had violated the sacrament of Read Only Friday. I was going through this process on a Friday night, and I was meeting up with my good friend [FreeRice](https://masto.borked.sh/@FreeRice) the next morning to showcase [options for creating a website](https://borked.sh/post/back-to-hugo/). While I could have used my [Pinebook Pro](https://borkedsh.medium.com/pinebook-pro-follow-up-manjaro-linux-eb79d8895dc9) for that, I knew it would be a better experience all around on a machine with more hardware behind it, so I _really_ needed this system to be working and didn't want to spend half the night fixing it. As a result, I stupidly his `Y` on the prompt to install all of he packages while hoping for the best.

After my machine updated for about 30 minutes with the fans blazing the entire time, the updates had completed. I did another reboot, anticipating kicking off the OS upgrade for a 3rd time afterward. While my machine booted to the login screen, the second I entered my credentials, my desktop opened with the virtual desktop switcher half open and immediately crashed. I had to hold in the power button and try again, which ended up with the exact same result. It seems fitting that my system at this point was extremely borked; I bought this domain name for a reason, after all.

## The Workaround

At this point I really had 2 options.

1. Try booting to recovery mode and start troubleshooting the problem.
2. Nuke and pave with a fresh OS installation.

Given the limited time I was working with and the fact that I had literally no data saved to the device that I was worried about keeping -- anything important was already stored elsewhere -- I went with option 2. However, rather than immediately downloading the 22.04 .iso of Pop!\_OS, I stepped back for a minute to consider my options. I was a _little_ salty that it had just let me down during the upgrade process, and I had been having concerns about continuing to use it going forward with respect to some of the decisions Ubuntu was making, such as doubling, tripling, and quadrupling down on horrible snap packages while ignoring the myriad of pitfalls surrounding them. Would those issues plague Pop!\_OS? If not now, then what about in the future when they're tied to Ubuntu as their base OS?

Ultimately, I decided that I would try something I hadn't used in a while and chose to install [Fedora](https://getfedora.org/en/workstation/), which I honestly don't think I had used in about a decade. Since my main laptop wasn't good for anything at the moment, I grabbed my trusty Pinebook, snagged the Fedora 35 .iso file, and mounted it to a USB drive. It took my poor little Pinebook a bit of time to churn through that process, but once it was done it was relatively quick and easy to get Fedora up and running. I'll do a separate post on that, but the short of it is that I've been loving Fedora so far. Maybe it was for the best that my machine borked itself!
