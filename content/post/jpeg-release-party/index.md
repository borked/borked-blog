---
title: "JPEG Release Party"
date: 2022-04-30T21:02:49-04:00
draft: false
---

I don't get NFTs. I don't understand why they exist, I really don't understand why people are willing to pay _any_ amount of money for them, and I **extremely** don't understand why people become obsessed with evangelizing them as if they're going to somehow revolutionize the world of... being able to tell people you "own" the rights to what is often mediocre digital artwork.

One of the things I don't miss from Twitter is seeing the web3-obsessed jumping to dog pile on anyone and everyone who says anything disparaging about it, regardless of [how much that person knows what they're taking about](https://twitter.com/gvanrossum/status/1508959260905918465). These days I mostly just see links shared on the Fediverse about the latest [hack/faux pas/failure of even the most basic security](https://web3isgoinggreat.com/) in some corner of the "metaverse" that has cost people millions of dollars.

Back when Twitter first released the ability to set an NFT as your profile picture (with a special shape to denote it as an NFT since, you know, you could _always_ just upload a digital image as your picture), I made an account on OpenSea just to see what it see what it would cost to upload an image of my own and "mint" it as an NFT that I sold to myself, hence at the lowest price possible. I had no intention of actually buying it, but I wanted to understand what the cost of this would even be. The things I learned were:

1. The cost fluctuates _wildly_ based on whatever random factors they use to determine how much money people need to waste on this stuff at a minimum. The price was anywhere from $50 - $80.
2. Any amount of money on this is too much since you're literally throwing it away to prove you own an image that you're just going to then upload to the Internet where everyone can right-click on it.

I laughed and then never even thought to close the OpenSea account with the little experiment concluded. Then, months later, I just now received this in my inbox:

![Screenshot of an email from OpenSea about some kind of NFT release party.](/post/jpeg-release-party/opensea.png)

The email said:

> The most highly-anticipated drop of 2022 is nearly here! Yuga Labs, the team behind the Bored Ape Yacht Club, is launching Otherside tonight at 9pm ET. Their latest project may be the most hyped so far and there has been tremendous excitement about the utility the Otherdeed NFTs will bring. To ensure you’re ready (with your countdown clock), we’ll be accepting ApeCoin or ETH for this launch - so load up your wallet and get your snacks ready. Buckle up and watch this explosive move into the metaverse!

I think the most telling part is "_so load up your wallet_", but the idea of people literally sitting around in anticipation waiting for the release of some JPEGs they can buy for actual money is just... bizarre.

I nearly unsubscribed from the mailing list, but I thought maybe I'd need another good laugh in the future.
