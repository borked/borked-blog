---
title: "iOS Protonmail App Update"
date: 2022-05-02T21:18:37-04:00
draft: false
---

I've been using [ProtonMail](https://protonmail.com/) as my main personal email account for quite a few years now. I've been supporting it as a paid member for most of that time, and it received the honor of being the first topic my friend and I covered in our -- long since defunct -- podcast. While I rather like the look of the [beta web app](https://beta.protonmail.com), I find the iOS app to be a bit on the bland side. In fact, the overall appearance of the application was one of the more popular pieces of feedback from the [2022 Proton survey](https://protonmail.com/blog/2022-proton-survey-insights/):

> A full 20% of respondents said that what they most want from ProtonMail is a more attractive/improved user interface. This is matched by the number of comments we receive on social media along the lines of “looks outdated but works” or “looks a bit boring, but gets the job done”.

That summary hits the nail on the head for me, especially with respect to the iOS app. It gets the job done, and the privacy aspects of the service are what I value the most. But if it _were_ to look a little nicer, I certainly wouldn't complain about it. This was clearly something the Proton team had already been working on prior to releasing the survey results, though, since just a week after that blog post they released a [brand new version of the iOS app](https://protonmail.com/blog/new-ios-app/).

## The Good

![Screenshot of the new ProtonMail iOS app showing the new interface and the difference between the light and dark modes.](/post/ios-protonmail-update/proton.png)

_Note: This is one of the few times where I'll just snag the screenshot from the blog post I'm referencing rather than creating my own just so that I don't have to blur out all of my email, thus making the screenshot less useful._

The most immediately noticeable thing is that there's now **dark mode support**. This was a _very_ welcome change for me since ProtonMail was one of the few apps installed on my phone that would casually burn out my retinas compared to everything else. Aside from that, though, the whole interface has been overhauled. Things just look much cleaner and more modern. Long-pressing messages now causes a bar to appear toward the bottom of the screen with options for doing things like deleting messages, marking them as read, or moving them to a different folder.

This works surprisingly better than having the same controls appear at the top of the screen, as I'm not forced to stretch my thumb up so far; the buttons I need are comfortably within reach.

## The Bad

This is a bit misleading, since there isn't really anything _wrong_ with the app. Rather, I was a little disappointed that a few things weren't included with it. The biggest for me is that the iPadOS app hasn't been updated yet, so it still looks the same way it has for the past several years. While the app is functional, the contrast between it and the newly refreshed iOS app is... stark. I'll be looking forward to getting a unified UI between the two platforms.

While the new dark mode looks good, what I'd really like to see is parity between the themes in the web app and themes in the mobile app. I'm a big fan of ProtonMail's Monokai theme, and I'd love to get the same experience on mobile:

![Screenshot of the themes available in the beta version of the ProtonMail web app, including Proton, Carbon, Snow, Monokai, Contrast, and Legacy.](/post/ios-protonmail-update/webmail_themes.png)

The final bit of disappointment, which I don't actually expect to be included in the ProtonMail app itself based on what Proton has done on Android, is the lack of any connection to the ProtonCalendar. I rather like their calendar offering, but the lack of a mobile app makes it more or less worthless to me. I used it heavily for the first few months of 2022 when I was interviewing frequently for new jobs. I kept track of my interviews and the connection information for the virtual meetings on ProtonCalendar, but the fact that my only means of getting notifications was to have ProtonCalendar open in a browser tab on my laptop wasn't exactly ideal. In 2022, I don't see a calendar being useful if I can't get notifications from it on my phone. Considering that I use ProtonMail for literally _all_ of my personal email, having to copy calendar events to my iCloud calendar just so that I'll get adequate reminders seems like a bit much.

On the whole, though, these are really either minor complaints or gripes that are only tangentially related to ProtonMail itself. I'm geeked with the terrific work the team has done, and I'm looking forward to seeing what else they create and improve in the future.
