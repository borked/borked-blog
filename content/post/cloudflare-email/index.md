---
title: "Cloudflare Email Redirect"
date: 2022-04-27T21:09:01-04:00
draft: false
---

When I had my [Mastodon instance](https://masto.borked.sh) set up, I had to provide an administrator email address for the [about page](https://masto.borked.sh/about/more). Suffice to say, I _didn't_ want my main personal email address showing up there. Instead, I figured an address on my `borked.sh` domain that simply redirected to my _real_ address would be better.

_Note: I swear the post about the Mastodon instance is coming. I just happened to stumble across some notes for **this** post that aren't stored with my normal blogging notes, so I figured I'd get it knocked out before it gets lost..._

Normally, I would create a redirect through my registrar, [Namecheap](https://www.namecheap.com/), since they offer that ability readily within their UI. However, while I registered `borked.sh` through Namecheap, it isn't serving DNS for the domain. That's being done by [Cloudflare](https://www.cloudflare.com/), which was required if I wanted to use an apex domain with [Cloudflare Pages](https://pages.cloudflare.com/). Cloudflare Pages is currently what's hosting my [main website](https://borked.sh/).

While I didn't immediately see any ability to create an email redirect in the never-ending mountain of Cloudflare settings, a quick search showed me that they offered this in what had -- relatively recently -- become an [open beta](https://blog.cloudflare.com/introducing-email-routing/). To get started with it, first I had to log in to Cloudflare and navigate to:

**Websites > borked.sh > Email**

As you can see, the **Email** option still even includes a "Beta" tag next to it:

![Screenshot of the main Cloudflare navigation menu showing the Email option as having a beta tag.](/post/cloudflare-email/beta_nav.png)

The first step was that it had me enter in what email address I wanted to create and the address that would ultimately end up _receiving_ the messages. After plugging in the `borked.sh` forwarding address I wanted to create and my normal [ProtonMail](https://protonmail.com/) address, Cloudflare sent a verification to ProtonMail. I clicked the verification link from the email which took me a page confirming the address was verified and offering _another_ link to go back to the next stage of email setup. However, clicking on this simply took me back to the screen telling me that I would receive a verification email at the "real" address I had entered. That seemed... not quite right. Refreshing the page made no difference, but fortunately there was a button at the bottom of the screen to "Skip verification", which I happily clicked. This took me to a screen letting me know that while my address was ready, my DNS records were missing. I just had to bounce back into the DNS settings for my domain and add the following `NS` records:

```
route1.mx.cloudflare.net
route2.mx.cloudflare.net
route3.mx.cloudflare.net
```

Plus a TXT record for SPF:

```
v=spf1 include:_spf.mx.cloudflare.net ~all
```

The SPF record seems a bit odd to me since I'll literally never send from this address; I don't even know how I _would_ send from it. The address I created literally just serves as a redirect. Regardless, after adding the DNS records, I tested sending a few emails to my new alias and confirmed that everything worked swimmingly.

Even better, accessing the same portion of the Cloudflare portal now not only shows me how everything is currently configured, but it also gives me a list of recent emails it received and forwarded. That's just nice to see in case I'm ever curious what email hit the alias and what was sent directly to my personal email. It also gives me the ability to create additional routes, add a catch-all address, and create additional destination addresses. I don't need _any_ of that at the moment, but as is so often the case, it's nice to [know I have the option should the need arise](https://blog.borked.sh/changing-medium-login-methods).

I assume that a lot of people are likely to not even read this post the second they see "Cloudflare" in the title -- not that my blog has the reach that "_a lot_" of people would see it... I'm not super popular --, but I've honestly been pretty happy with their Pages offering. While I don't necessarily love the fact that I have to make Cloudflare responsible for my DNS to use an apex domain with it, their management of DNS isn't something I can come up with any complaints to.
