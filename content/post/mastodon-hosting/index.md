---
title: "Mastodon Hosting"
date: 2022-04-30T21:02:49-04:00
draft: false
---

As I've mentioned in a [couple](https://blog.borked.sh/git-commit-metrics) of [posts](https://blog.borked.sh/cloudflare-email-redirect), I recently started a [Mastodon instance](https://masto.borked.sh). I've used Mastodon for quite a while (and GNU Social before that), starting over at [mstdn.io](https://mstdn.io/about) originally before heading to [SDF](https://mastodon.sdf.org/about), followed by [Fosstodon](https://fosstodon.org/about). The moves had nothing to do with those instances, each of which was great, but more with the normal ebb and flow of my creating and subsequently deleting social media accounts.

With Twitter seeming like even more of a cesspool than normal at the start of this year, I decided to once again take part in the Fediverse by creating an account over at [mastodon.online](https://mastodon.online/@borked), one of the "mega instances" run by the core developers of Mastodon. With the news of Elon Musk buying a 9% chunk of Twitter, though, I saw an opportunity to get some of my friends who wouldn't normally consider Mastodon in lieu of Twitter to potentially reconsider. While I didn't think most people I knew would be particularly inclined to join a random Mastodon instance, I figured they would probably join _my_ Mastodon instance if I had one.

## Hosting

The main problem is that I didn't actually want the responsibility of running the infrastructure for Mastodon. While it would be fairly simple to get something up and running with the official [Docker image](https://hub.docker.com/r/tootsuite/mastodon), I really didn't want to deal with maintaining it, installing updates to the host -- if I didn't do some type of k8s PaaS -- taking backups, etc. Then I happened to stumble across a thread where [kev](https://fosstodon.org/@kev), the creator of Fosstodon, mentioned that his instance was run through [Masto.host](https://masto.host/). The site's tagline says it all:

> fully managed Mastodon hosting

I've seen some Mastodon hosting options before, but all of them were prohibitively expensive for what I needed: a small instance that would house just a handful of local users. Masto.host, however, offers a [diverse suite of options](https://masto.host/pricing/), with plans ranging from 6€ a month all the way up to 89€ a month -- or more if you need something custom above and beyond that. Included is the hosting, the ability to use your own domain, installation and updates, and backups. Checking the sizes, I figured I could either go with the 6€ plan and be very choosy about who I allow on my instance or bump up to the 10€ plan and offer up accounts to a slightly broader set of friends.

After dropping some messages in group chats to get buy-in that at least a handful of people would actually join, I went ahead and opted in to the 10€ plan.

## Buying and Provisioning

After clicking the button to buy an instance, I almost immediately got an email from [Hugo](https://masto.pt/@hugo), the person who runs Masto.host. This wasn't an automated email; they're just really on top of things. Hugo asked me to make some DNS changes for my custom domain so the instance could be deployed, all of the information for which was included in the message. After a slight delay caused on my side by the fact that I forgot to turn off [Cloudflare's](https://blog.borked.sh/cloudflare-email-redirect) DNS proxy, my instance was up and running in under 30 minutes. I got another email from Hugo asking me to create an account so that it could be set as the admin.

That was all it took, and everything was done insanely quickly on a Friday evening. I can't say enough about the top-notch service from Masto.host, and the instance has been running beautifully, all for less than I could've possibly paid for a VPS that would be capable of running it. I'd highly recommend Masto.host for anyone considering an instance but who either doesn't have the technical ability to do it themselves or, like me, doesn't want to deal with the upkeep.

## Setup

With admin access to my instance, I needed to handle some configuration. Masto.host provides a [handy guide](https://masto.host/mastodon-server-first-steps/) for new admins with some things that it's good to configure. Key for me was configuring my instance to only allow account creation by request; given that my instance is sized for no more than 20 people, I certainly didn't want anyone random registering and using resources. With that handled, I just needed to customize the instance with my own graphics, write a quick description, and [block the instances that the major ones block](https://mastodon.social/about/more).

## Aftermath

It's been just over 2 weeks since my instance has been created, and on the whole I think things are going pretty well. Obviously, in the time between creation and now, further news came about regarding Elon Musk's offer to completely buy Twitter and Twitter's subsequent acceptance of that offer, which only makes Mastodon all the more attractive for some of the friends in my circle. I would say that the number of people who ended up signing up for my instance was a little lower than I had anticipated, but the friends who _did_ sign up seem to be enjoying it. One person even sent me a -- much appreciated -- donation to cover 2 months' worth of hosting!

I think one of the challenging things for new people getting started with Mastodon is actually finding people to follow and engage with. Having used the platform for quite a while, I was easily able to create a new account, follow a handful of people, and I already have interesting engagement on a lot of my posts. Without creepy algorithms shoving content at you, though, it's important for new users to understand that it may take a bit more time than they're expecting to start to build a decent list of interesting people to follow. I think at least having our own instance helps some of my friends with that, though, since if nothing else we can always engage with one another. Obviously we could have done the same if we all had accounts on other instances, but there's something about being together on your own instance that I think makes things a bit easier for newcomers.

Happy tooting! And feel free to [give me a follow](https://masto.borked.sh/@borked) if you're also on Mastodon!
